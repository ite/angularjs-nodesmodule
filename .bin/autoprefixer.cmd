@IF EXIST "%~dp0\node.exe" (
  "%~dp0\node.exe"  "%~dp0\..\grunt-autoprefixer\node_modules\autoprefixer\bin\autoprefixer" %*
) ELSE (
  node  "%~dp0\..\grunt-autoprefixer\node_modules\autoprefixer\bin\autoprefixer" %*
)