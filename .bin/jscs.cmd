@IF EXIST "%~dp0\node.exe" (
  "%~dp0\node.exe"  "%~dp0\..\jscs\bin\jscs" %*
) ELSE (
  node  "%~dp0\..\jscs\bin\jscs" %*
)